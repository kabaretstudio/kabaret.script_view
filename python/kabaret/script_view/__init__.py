from .script_view import ScriptView

from . import _version
__version__ = _version.get_versions()['version']